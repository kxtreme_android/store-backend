import { User } from '../models/user.js'
export async function auth(req, res) {
  const authorizationHeader = req.headers.authorization
  const tokenParts = authorizationHeader.split(' ')
  if (tokenParts.length < 1) {
    res.status(403).send()
    return
  }
  const token = tokenParts[1]
  const users = await User.findAll({
    where: {
      token
    }
  })
  if (users.length < 1) {
    throw 'User not found'
  }
  return users[0]
}
