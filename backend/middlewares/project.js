import { auth } from './auth.js'
export async function getProject(req, res) {
  try {
    const user = await auth(req, res)
    const bodyProjectId = req.body.projectId
    const paramsProjectId = req.params.projectId
    const projectId = bodyProjectId ?? paramsProjectId
    const userProjects = await user.getProjects()
    return userProjects.find((userProject) => userProject.id == projectId)
  } catch (e) {
    return null
  }
}

export async function hasWritePermission(req, res) {
  const project = await getProject(req, res)

  if (project == null) {
    return false
  }

  return ['admin', 'write'].includes(project.User_Project.type)
}

export async function hasReadPermission(req, res) {
  const project = await getProject(req, res)
  console.log(project.type)

  if (project == null) {
    return false
  }

  return ['admin', 'write', 'read'].includes(project.User_Project.type)
}
