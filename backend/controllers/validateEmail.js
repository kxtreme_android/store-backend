import bcrypt from 'bcrypt'
import { User } from '../models/user.js'
import { auth } from '../middlewares/auth.js'

const validateEmail = (email) => {
  return String(email)
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )
}
export const AuthenticationController = {
  login: async (req, res) => {
    const params = req.body
    const users = await User.findAll({
      where: {
        email: params.email
      }
    })
    if (users.length < 1) {
      res.status(403).send()
      return
    }
    const user = users[0]

    if (!bcrypt.compareSync(params.password, user.password)) {
      res.status(403).send()
      return
    }
    for (var token = ''; token.length < 100; ) token += Math.random().toString(36).substr(2, 1)
    await User.update(
      { token },
      {
        where: {
          id: user.id
        }
      }
    )
    res.send({ token })
  },
  register: (req, res) => {
    const params = req.body
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync(params.password, salt)

    const email = params.email
    if (!validateEmail(email)) {
      res.status(401).send('invalid email format')
      return
    }

    User.create({
      email,
      password: hash
    })
      .then((user) => {
        user.password = undefined
        res.send(user)
      })
      .catch(() => {
        res.status(403).send('error creating the user')
      })
  },
  me: (req, res) => {
    auth(req, res)
      .then((user) => {
        user.password = undefined
        user.token = undefined
        res.send(user)
      })
      .catch(() => {
        res.status(403).send()
      })
  },
  logout: (req, res) => {
    auth(req, res)
      .then((user) => {
        User.update(
          { token: '' },
          {
            where: {
              id: user.id
            }
          }
        )
        res.send()
      })
      .catch(() => {
        res.status(403).send()
      })
  }
}
