import { Category } from '../models/category.js'
import { getProject, hasReadPermission, hasWritePermission } from '../middlewares/project.js'

export const CategoryController = {
  all: async (req, res) => {
    if (!hasReadPermission(req, res)) {
      res.status(403).send()
      return
    }

    try {
      const project = await getProject(req, res)
      const categories = await Category.findAll({
        where: {
          ProjectId: project.id
        }
      })
      res.send(categories)
    } catch (e) {
      res.status(403).send()
    }
  },
  create: async (req, res) => {
    if (!hasWritePermission(req, res)) {
      res.status(403).send()
      return
    }

    try {
      const project = await getProject(req, res)
      const category = await Category.create({
        ...req.body,
        ProjectId: project.id
      })
      res.send(category)
    } catch (e) {
      console.error(e)
      res.status(403).send()
    }
  }
}
