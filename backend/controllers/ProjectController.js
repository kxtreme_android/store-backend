import { Project } from '../models/project.js'
import { auth } from '../middlewares/auth.js'

export const ProjectController = {
  all: async (req, res) => {
    try {
      const user = await auth(req, res)

      res.send(await user.getProjects())
    } catch (e) {
      res.status(403).send(e)
    }
  },
  create: async (req, res) => {
    try {
      const user = await auth(req, res)
      console.log(user.id)
      const project = await Project.create(req.body)
      project.addUser(user, { through: { type: 'admin' } })
      res.send(project)
    } catch (e) {
      res.status(403).send()
    }
  }
}
