import { Item } from '../models/item.js'
import { getProject, hasReadPermission, hasWritePermission } from '../middlewares/project.js'

export const ItemController = {
  all: async (req, res) => {
    if (!(await hasReadPermission(req, res))) {
      res.status(403).send()
      return
    }

    try {
      const project = await getProject(req, res)
      const items = await Item.findAll({
        where: {
          ProjectId: project.id
        }
      })
      res.send(items)
    } catch (e) {
      console.error(e)
      res.status(403).send()
    }
  },
  create: async (req, res) => {
    if (!(await hasWritePermission(req, res))) {
      res.status(403).send()
      return
    }

    try {
      const project = await getProject(req, res)
      const item = await Item.create({
        ...req.body,
        ProjectId: project.id
      })
      res.send(item)
    } catch (e) {
      res.status(403).send()
    }
  }
}
