import { DataTypes } from 'sequelize'
import { sequelize } from './database.js'

export const Item = sequelize.define('Item', {
  // Model attributes are defined here
  name: {
    type: DataTypes.STRING
  }
})
