import { sequelize } from './database.js'
import { DataTypes } from 'sequelize'
import { Project } from './project.js'
import { User } from './user.js'
import { Category } from './category.js'
import { Item } from './item.js'

const User_Project = sequelize.define(
  'User_Project',
  {
    type: {
      type: DataTypes.ENUM('admin', 'write', 'read'),
      defaultValue: 'read'
    }
  },
  { timestamps: false }
)

export const defineRelationship = () => {
  User.belongsToMany(Project, { through: User_Project })
  Project.belongsToMany(User, { through: User_Project })

  Category.belongsToMany(Item, { through: 'Category_Item' })
  Item.belongsToMany(Category, { through: 'Category_Item' })

  Project.hasMany(Category)
  Project.hasMany(Item)

  Category.belongsTo(Project)
  Item.belongsTo(Project)
}
