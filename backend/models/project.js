import { DataTypes } from 'sequelize'
import { sequelize } from './database.js'

export const Project = sequelize.define('Project', {
  // Model attributes are defined here
  name: {
    type: DataTypes.STRING
  }
})
