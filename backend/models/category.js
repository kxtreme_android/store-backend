import { DataTypes } from 'sequelize'
import { sequelize } from './database.js'

export const Category = sequelize.define('Category', {
  // Model attributes are defined here
  identifier: {
    type: DataTypes.STRING
  },
  name: {
    type: DataTypes.STRING
  }
})
