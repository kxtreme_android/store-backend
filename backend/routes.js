import { AuthenticationController } from './controllers/validateEmail.js'
import { ProjectController } from './controllers/ProjectController.js'
import { CategoryController } from './controllers/CategoryController.js'
import { ItemController } from './controllers/ItemController.js'

export const RouteManager = {
  setRoutes: (app) => {
    app.post('/api/login', (req, res) => {
      AuthenticationController.login(req, res)
    })
    app.post('/api/register', (req, res) => {
      AuthenticationController.register(req, res)
    })
    app.post('/api/logout', (req, res) => {
      AuthenticationController.logout(req, res)
    })
    app.get('/api/me', (req, res) => {
      AuthenticationController.me(req, res)
    })

    app.get('/api/projects', (req, res) => {
      ProjectController.all(req, res)
    })
    app.post('/api/projects', (req, res) => {
      ProjectController.create(req, res)
    })
    app.get('/api/projects/:projectId/categories', (req, res) => {
      CategoryController.all(req, res)
    })
    app.post('/api/projects/:projectId/categories', (req, res) => {
      CategoryController.create(req, res)
    })
    app.get('/api/projects/:projectId/items', (req, res) => {
      ItemController.all(req, res)
    })
    app.post('/api/projects/:projectId/items', (req, res) => {
      ItemController.create(req, res)
    })
  }
}
