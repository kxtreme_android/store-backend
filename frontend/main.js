import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router/main'
import axios from './plugins/axios'
import Vue3Datatable from '@bhplugin/vue3-datatable'
import "@bhplugin/vue3-datatable/dist/style.css";
import 'element-plus/es/components/message/style/css'
import 'element-plus/theme-chalk/dark/css-vars.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(axios, {
    baseUrl: import.meta.env.VITE_API_ENDPOINT,
})

app.component("DataTable", Vue3Datatable)

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
  }

app.mount('#app')
