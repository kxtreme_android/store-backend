import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: '/', redirect: '/auth' },
    {
      path: '/auth',
      name: 'auth',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('../views/HomeView.vue'),
      children: [
        {
          path: 'projects',
          name: 'projects',
          component: () => import('../views/ProjectsPage.vue'),
          children: [
            {
              path: 'add',
              name: 'projects,add',
              component: () => import('../views/ProjectsAddPage.vue')
            }
          ]
        },

        {
          path: 'categories',
          name: 'categories',
          component: () => import('../views/CategoriesPage.vue'),
          children: [
            {
              path: 'add',
              name: 'categories,add',
              component: () => import('../views/CategoriesAddPage.vue')
            }
          ]
        },
        {
          path: 'items',
          name: 'items',
          component: () => import('../views/ItemsPage.vue'),
          children: [
            {
              path: 'add',
              name: 'items,add',
              component: () => import('../views/ItemsAddPage.vue')
            }
          ]
        }
      ]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    }
  ]
})

export default router
