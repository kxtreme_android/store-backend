import { useAuthTokenStore } from '@/stores/authToken';
import axios from 'axios'
export default {
    install: (app, options) => {
         const $axios = axios.create({
            baseURL: options.baseUrl
        })
        $axios.interceptors.request.use(function (config) {
            config.headers["Authorization"] = useAuthTokenStore().bearerToken
            return config
        })

        app.config.globalProperties.$axios = $axios
    }
}