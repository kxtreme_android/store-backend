import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useUserStore = defineStore('user', () => {
  const userRef = ref()

  const user = computed(() => userRef.value)
  const  setUser = (user) =>{
    userRef.value = user
  }

  return { user, setUser }
})
