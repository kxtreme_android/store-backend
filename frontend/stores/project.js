import { toRef, computed } from 'vue'
import { defineStore } from 'pinia'

export const useProjectStore = defineStore('project', () => {
  const storedSelectedProject = localStorage.getItem('selectedProject') ?? false
  const selectedProjectRef = toRef(storedSelectedProject)

  const selectedProject = computed(() => selectedProjectRef.value)
  const setSelectedProject = (projectId) => {
    selectedProjectRef.value = projectId
    localStorage.setItem('selectedProject', projectId)
  }

  return { selectedProjectRef: { ref: selectedProjectRef }, selectedProject, setSelectedProject }
})
