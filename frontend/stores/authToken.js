import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useAuthTokenStore = defineStore('authToken', () => {
  const authToken = ref(localStorage.getItem("token") ?? "")
  const bearerToken = computed(() => `Bearer ${authToken.value}`)
  const  setToken = (token) =>{
    authToken.value = token
    localStorage.setItem("token", token);
  }

  return { authToken, bearerToken, setToken }
})
