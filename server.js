import 'dotenv/config'
import * as path from 'path'
import express from 'express'
const app = express()
import bodyParser from 'body-parser'
import cors from 'cors'
const port = process.env.SERVER_PORT
import { RouteManager } from './backend/routes.js'
import { sequelize } from './backend/models/database.js'
import { defineRelationship } from './backend/models/relations.js'
import { fileURLToPath } from 'url'

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
RouteManager.setRoutes(app)
app.use('/', express.static(path.join(__dirname, 'dist')))
defineRelationship()

sequelize
  .sync
  //{force: true}
  ()
  .then(() => console.log('tables syncronized'))
  .catch(console.error)

app.listen(port, () => {
  console.log(`Server available at port :${port}`)
})
